package SRPtest;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

import junit.framework.TestCase;
import model.logic.SistemaRecomendacionPeliculas;

public class SRPTest extends TestCase
{

	private SistemaRecomendacionPeliculas srp;
	
	public void setupEscenario1()
	{
		File ruta = new File( "./Material de apoyo/ml-latest-small/movies.csv" );
		String path =  ruta.getAbsolutePath();
		if( path.startsWith("C:/"))
		{
			fail( "Ruta inicial inválida");
		}
		srp = new SistemaRecomendacionPeliculas();
	}
	
	public void cargarPeliculasTest( )
	{
		setupEscenario1();
		
		String ruta = "./Material de apoyo/ml-latest-small/movies.csv";
		srp.cargarPeliculasSR(ruta);
		try
		{
			CSVReader reader = new CSVReader(new FileReader(ruta));
			String [] nextLine = reader.readNext();
		    while ((nextLine = reader.readNext()) != null) 
		    {
		    	for( int i = 0; i < srp.getPeliculas().darNumeroElementos(); i++ )
				{
					assertEquals( "El id no corresponde", nextLine[0], srp.getPeliculas().darElemento(i) );
				}
		    }
		}
		catch( IOException e)
		{
			System.out.println("Error en la lectura del archivo" + e.getStackTrace());
		}
		
		
	}
}
