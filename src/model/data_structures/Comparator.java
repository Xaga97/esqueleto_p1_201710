package model.data_structures;

public interface Comparator<T extends Comparable<T>> 
{
	public abstract int compare(T t1, T t2);
}
