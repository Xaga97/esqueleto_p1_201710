package model.data_structures;

import java.util.Iterator;


public class Queue<T extends Comparable<T>> 
{
	 private ListaEncadenada<T> base;
	 public Queue() {
	  base=new ListaEncadenada<T>();
	 }
	 public void enqueue(T item){
	  base.agregarElementoFinal(item);
	 }
	 public T dequeue(){
	  T joder=base.darElemento(0);
	  base.eliminarInicial();
	  base.currentSize--;
	  return joder;
	 }
	 public int size()
	 {
	  return base.currentSize;
	 }
	 public boolean isEmpty(){
	  if(base.currentSize==0)
	   return true;
	  else return false;
	 }
	public T darPrimero() {
		return base.darPrimero();
	}
	}