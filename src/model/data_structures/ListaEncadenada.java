package model.data_structures;

import java.util.Iterator;

import model.data_structures.ListaDobleEncadenada.Casilla;

public class ListaEncadenada<T extends Comparable<T>> implements ILista<T> 
{
	Casilla inicial;
	int currentSize;
	Casilla actual;
	Casilla fin;
	public ListaEncadenada<T> aux;
	
	public ListaEncadenada() {
		// TODO Auto-generated constructor stub

		actual=inicial = null;
		currentSize = 0;

	}
	public static class Casilla<T> {
		private T elemento;
		private Casilla siguiente;

		public Casilla(T pElemento) {
			// TODO Auto-generated constructor stub
			elemento = pElemento;
			siguiente = null;
		}

		public Casilla darSiguiente() {
			return siguiente;
		}

		public void setSig(Casilla c) {
			this.siguiente = c;
		}

		public T darElemeto() {
			return elemento;
		}

	}

	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new MyIterator(inicial);
	}

	private static class MyIterator<T> implements Iterator{

		private Casilla actual;
		public MyIterator(Casilla inicial) {
			// TODO Auto-generated constructor stub
			actual = inicial;	
		}
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return actual.darSiguiente() != null ? true : false;
		}
		public Object next() {
			// TODO Auto-generated method stub
			return actual.darSiguiente();
		}
		@Override
		public void remove() {
			// TODO Auto-generated method stub

		}
	}
	public Casilla getInicial() {
		return inicial;
	}

	public void setInicial(T element) {
		this.inicial = new Casilla(element);
	}
	@Override
	public void agregarElementoFinal(T elem) {
		Casilla casillaActual = inicial;
		if (inicial == null){
			inicial = new Casilla<T>(elem);
			actual=inicial;
		}
		else {
			for (; casillaActual.darSiguiente() != null;) {
				casillaActual = casillaActual.darSiguiente();
			}
			casillaActual.setSig(new Casilla<T>(elem));
		}
		currentSize++;

	}

	@Override
	public T darElemento(int pos) {
		Casilla casillaActual = inicial;
		for (int i = 0; i < pos; i++)
			try {
				casillaActual = casillaActual.darSiguiente();	
			} catch (Exception e) {
				// TODO: handle exception
				casillaActual=null;
				break;
			}
		return (T) casillaActual.darElemeto();
	}


	@Override
	public int darNumeroElementos() {
		return currentSize;
	}


	@Override
	public T eliminarElemento(int pos){
		if (darElemento(pos) == null) {
			return null;
		} else {
			if (pos == 0) {
				inicial = inicial.siguiente;
				currentSize--;
			} else {
				@SuppressWarnings("rawtypes")
				Casilla anterior = (Casilla) darElemento(pos - 1);
				@SuppressWarnings("rawtypes")
				Casilla siguiente = (Casilla) darElemento(pos + 1);
				anterior.setSig(siguiente);
				currentSize--;
			}
		}
		return darElemento(pos);
	}

	public void clear() {
		actual=inicial = null;
		currentSize = 0;
		
	}
	public void eliminarInicial() {
		inicial=inicial.darSiguiente();
		
	}

	public void eliminarFinal() {
		int i = 0;
		Casilla casillaActual = inicial;
		if(currentSize>=2){
		while(i<currentSize-2){
			casillaActual=casillaActual.darSiguiente();
			i++;
		}
		casillaActual.setSig(null);
		fin=casillaActual;
		}
		else inicial=fin=null;
	}

	public T darUlt() {
		return (T) fin.darElemeto();
		
	}

	public T darPrimero() {
		return (T) inicial.darElemeto();
	}
	
	public T buscarElemento( T elem )
	{
		boolean encontro = false;
		Casilla<T> casActual = inicial;
		T buscado = null;
		for( int i = 0; i < currentSize && !encontro; i++ )
		{
			try 
			{
				if( ( casActual.darElemeto()).equals(elem))
				{
					buscado = casActual.darElemeto();
					encontro = true;
				}
				else
				{
					casActual = casActual.darSiguiente();	
				}
			} 
			catch (Exception e) 
			{
				casActual=null;
				break;
			}
		}
		return buscado;
	}
	
	public void agregarElementoPosicion( int pos, T elem )
	{
		Casilla<T> casillaActual = inicial;
		Casilla<T> casillaAnterior = null;
		for (int i = 0; i < pos; i++)
		{
			if( i == (pos-1) )
			{
				Casilla<T> agregar = new Casilla<T>( elem );
				agregar.setSig(casillaActual);
				casillaAnterior.setSig(agregar);
			}
			casillaAnterior = casillaActual;
			casillaActual = casillaActual.darSiguiente();	
		}

	}
	
	/**
	 * Método que organiza una lista con el algoritmo MergeSort
	 * @param a Lista que se quiere ordenar
	 * @param comp Comparador del tipo que se requiera para comparar los elementos de la lista 
	 * @param order True para orden ascendente, False para orden descendente
	 */
	public void sort(ILista<T> a, Comparator<T> comp, boolean order )
	{
		aux = new ListaEncadenada<T>(); 
		sort(comp, 0, a.darNumeroElementos() - 1, a, order);
	}
	

	private void sort(Comparator<T> comp, int lo, int hi, ILista<T> a, boolean order) 
	{
		if (hi <= lo) 
			return;
		 int mid = lo + (hi - lo)/2;
		 sort(comp, lo, mid, a, order); // Sort left half.
		 sort(comp, mid+1, hi, a, order); // Sort right half.
		 merge(a, lo, mid, hi, comp, order);
	}
	
	private void merge(ILista<T> a, int lo, int mid, int hi, Comparator<T> comp, boolean order)
	{
		int i = lo, j = mid+1;
		for (int k = lo; k <= hi; k++) 
			aux.agregarElementoPosicion(k, a.darElemento(k));
		for (int k = lo; k <= hi; k++)
		{
			if( order )
			{
				if (i > mid) 
				{
					a.agregarElementoPosicion(k, aux.darElemento(j++));
				}
				else if (j > hi ) 
				{
					a.agregarElementoPosicion(k, aux.darElemento(i++));
				}
				else if( comp.compare(aux.darElemento(j), aux.darElemento(i)) < 0 ) 
				{ 
					a.agregarElementoPosicion(k, aux.darElemento(j++));
				}
				else 
				{
					a.agregarElementoPosicion(k, aux.darElemento(i++));
				}
			}
			else
			{
				if (i > mid) 
				{
					a.agregarElementoPosicion(k, aux.darElemento(j++));
				}
				else if (j > hi ) 
				{
					a.agregarElementoPosicion(k, aux.darElemento(i++));
				}
				else if( comp.compare(aux.darElemento(j), aux.darElemento(i)) > 0 ) 
				{ 
					a.agregarElementoPosicion(k, aux.darElemento(j++));
				}
				else 
				{
					a.agregarElementoPosicion(k, aux.darElemento(i++));
				}
			}
		}
	}
	
	/**
	 * Método que organiza una lista con el algoritmo MergeSort, se usa cuando se quiere 
	 * ordenarr dados 3 criterios cuando hay dos elemntos que tienen igual el primero o segundo criterio 
	 * NOTA: Los criterios deben ir en orden de prioridad según lo deseado
	 * @param a Lista que se quiere ordenar
	 * @param compPrimerCriterio Comparador del tipo del primer criterio que se requiera para comparar los elementos de la lista
	 * @param compSegundoCriterio Comparador del tipo del segundo criterio
	 * @param compTercerCriterio Comparador del tipo del tercer criterio  
	 * @param order True para orden ascendente, False para orden descendente
	 */
	public void sort3Criterios( ILista<T> a, Comparator<T> compPrimerCriterio, Comparator<T> compSegundoCriterio, Comparator<T> compTercerCriterio, boolean order )
	{
		aux = new ListaEncadenada<T>(); 
		sort3Criterios(compPrimerCriterio, compSegundoCriterio, compTercerCriterio, 0, a.darNumeroElementos() - 1, a, order);
	}
	
	private void sort3Criterios( Comparator<T> comp1, Comparator<T> comp2, Comparator<T> comp3, int lo, int hi, ILista<T> a, boolean order )
	{
		if (hi <= lo) 
			return;
		 int mid = lo + (hi - lo)/2;
		 sort3Criterios(comp1, comp2, comp3, lo, mid, a, order); // Sort left half.
		 sort3Criterios(comp1, comp2, comp3, mid+1, hi, a, order); // Sort right half.
		 merge3Criterios(a, lo, mid, hi, comp1, comp2, comp3, order);
	}
	
	private void merge3Criterios(ILista<T> a, int lo, int mid, int hi, Comparator<T> comp1, Comparator<T> comp2, Comparator<T> comp3, boolean order)
	{
		int i = lo, j = mid+1;
		for (int k = lo; k <= hi; k++) 
			aux.agregarElementoPosicion(k, a.darElemento(k));
		for (int k = lo; k <= hi; k++)
		{
			if( order )
			{
				if (i > mid) 
				{
					a.agregarElementoPosicion(k, aux.darElemento(j++));
				}
				else if (j > hi ) 
				{
					a.agregarElementoPosicion(k, aux.darElemento(i++));
				}
				else if( comp1.compare(aux.darElemento(j), aux.darElemento(i)) == 0 )
				{
					if( comp2.compare(aux.darElemento(j), aux.darElemento(i)) == 0 )
					{
						if( comp3.compare(aux.darElemento(j), aux.darElemento(i)) < 0 )
						{
							a.agregarElementoPosicion(k, aux.darElemento(j++));
						}
						else
						{
							a.agregarElementoPosicion(k, aux.darElemento(i++));
						}
					}
					else if( comp2.compare(aux.darElemento(j), aux.darElemento(i)) < 0 )
					{
						a.agregarElementoPosicion(k, aux.darElemento(j++));
					}
					else
					{
						a.agregarElementoPosicion(k, aux.darElemento(i++));
					}
				}
				else if( comp1.compare(aux.darElemento(j), aux.darElemento(i)) < 0 ) 
				{ 
					a.agregarElementoPosicion(k, aux.darElemento(j++));
				}
				else 
				{
					a.agregarElementoPosicion(k, aux.darElemento(i++));
				}
			}
			else
			{
				if (i > mid) 
				{
					a.agregarElementoPosicion(k, aux.darElemento(j++));
				}
				else if (j > hi ) 
				{
					a.agregarElementoPosicion(k, aux.darElemento(i++));
				}
				else if( comp1.compare(aux.darElemento(j), aux.darElemento(i)) == 0 )
				{
					if( comp2.compare(aux.darElemento(j), aux.darElemento(i)) == 0 )
					{
						if( comp3.compare(aux.darElemento(j), aux.darElemento(i)) > 0 )
						{
							a.agregarElementoPosicion(k, aux.darElemento(j++));
						}
						else
						{
							a.agregarElementoPosicion(k, aux.darElemento(i++));
						}
					}
					else if( comp2.compare(aux.darElemento(j), aux.darElemento(i)) > 0 )
					{
						a.agregarElementoPosicion(k, aux.darElemento(j++));
					}
					else
					{
						a.agregarElementoPosicion(k, aux.darElemento(i++));
					}
				}
				else if( comp1.compare(aux.darElemento(j), aux.darElemento(i)) > 0 ) 
				{ 
					a.agregarElementoPosicion(k, aux.darElemento(j++));
				}
				else 
				{
					a.agregarElementoPosicion(k, aux.darElemento(i++));
				}
			}
		}
	}
	
	public void cambiarActualPrimero( )
	{
		actual = inicial;
	}
	
	
}

