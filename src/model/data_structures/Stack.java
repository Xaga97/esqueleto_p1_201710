package model.data_structures;

import java.util.Iterator;

public class Stack<T> {
	 private ListaEncadenada<T> base;
	 public Stack() {
	  base=new ListaEncadenada<>();
	 }
	 public void push(T item){
	  base.agregarElementoFinal(item);
	 }
	 public T pop()
	 {
	  T temo=base.darElemento(base.currentSize-1);
	  base.eliminarFinal();
	  base.actual=base.inicial;
	  return temo;
	 }
	 public boolean isEmpty(){
	  if(base.currentSize==0)
	   return true;
	  else
	   return false;
	 }
	 public int size(){
	  return base.currentSize;
	 }
	public void clear() {
		base.clear();
		
	}
	public T darUlt() {
		return base.darUlt();
	}
	}