package model.data_structures;

public interface ILista<T extends Comparable<T>> extends Iterable<T>{
	
	
	
	public void agregarElementoFinal(T elem);
	
	public T darElemento(int pos);
	
	public T eliminarElemento(int pos);
	
	public int darNumeroElementos();
	
	public T buscarElemento( T elem );
	
	public void sort(ILista<T> a, Comparator<T> comp, boolean order );
	
	public void sort3Criterios( ILista<T> a, Comparator<T> compPrimerCriterio, Comparator<T> compSegundoCriterio, Comparator<T> compTercerCriterio, boolean order );
	
	public void agregarElementoPosicion( int pos, T elem );
	
	public void cambiarActualPrimero( );
	

}
