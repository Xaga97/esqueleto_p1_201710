package model.vo;

import model.data_structures.Comparator;
import model.data_structures.ILista;

public class VOUsuarioConteo implements Comparable<VOUsuarioConteo> {
	private long idUsuario;
	private int conteo;
	private ILista<VOTag> tagsAsociados;
	
	public long getIdUsuario() 
	{
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getConteo() {
		return conteo;
	}
	public void setConteo(int conteo) {
		this.conteo = conteo;
	}
	public ILista<VOTag> getTags()
	{
		return tagsAsociados;
	}
	public void setTags( ILista<VOTag> tags )
	{
		this.tagsAsociados = tags;
	}
	
	@Override
	public int compareTo(VOUsuarioConteo o) 
	{
		if( idUsuario == o.getIdUsuario() )
		{
			return 0;
		}
		else if( idUsuario > o.getIdUsuario() )
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
	
	public ComparatorbyConteo compardorConteo(){
		return new ComparatorbyConteo();
	}
	
	public static class ComparatorbyConteo implements Comparator<VOUsuarioConteo>
	{
		public int compare(VOUsuarioConteo t1, VOUsuarioConteo t2) 
		{
			if(t1.getConteo() > t2.getConteo() )
			{
				return 1;
			}
			else if( t1.getConteo() < t2.getConteo() )
			{
				return -1;
			}
			else
			{
				return 0;
			}
		}
		
	}
	
}
