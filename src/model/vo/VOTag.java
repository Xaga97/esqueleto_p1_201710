package model.vo;

import model.data_structures.Comparator;

public class VOTag implements Comparable<VOTag>
{
	private String tag;
	private long timestamp;
	private long IDUsuario;
	private long IDPelicula;
	
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public void setIDUsuario( long id )
	{
		this.IDUsuario = id;
	}
	public long getIDUsuario( )
	{
		return IDUsuario;
	}
	public void setIDPelicula( long id )
	{
		this.IDPelicula = id;
	}
	public long getIDPelicula( )
	{
		return IDPelicula;
	}
	@Override
	public int compareTo(VOTag o) 
	{
		return tag.compareTo( o.getTag() );
	}
	
	public ComparatorbyNombre compardorNombre(){
		return new ComparatorbyNombre();
	}
	
	public static class ComparatorbyNombre implements Comparator<VOTag>
	{
		public int compare(VOTag t1, VOTag t2) 
		{
			return (t1.getTag()).compareToIgnoreCase(t2.getTag());
		}
		
	}
	
	public ComparatorbyMasReciente compardorMasReciente(){
		return new ComparatorbyMasReciente();
	}
	
	public static class ComparatorbyMasReciente implements Comparator<VOTag>
	{
		public int compare(VOTag t1, VOTag t2) 
		{
			if( t1.getTimestamp() > t2.getTimestamp() )
			{
				return 1;
			}
			else if( t1.getTimestamp() < t2.getTimestamp() )
			{
				return -1;
			}
			else
			{
				return 0;
			}
		}
		
	}
}
