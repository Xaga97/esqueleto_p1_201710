package model.vo;

import model.data_structures.Comparator;
import model.data_structures.ILista;

public class VOPelicula implements Comparable<VOPelicula> 
{
	
	private long idPelicula; 
	private String titulo;
	private int numeroRatings;
	private int numeroTags;
	private double promedioRatings;
	private int agnoPublicacion;
	private double diferencia;

	private ILista<VOTag> tagsAsociados;
	private ILista<String> generosAsociados;
	
	
	public long getIdPelicula( )
	{
		return idPelicula;
	}
	public void setIdPelicula( long id )
	{
		this.idPelicula = id;
	}
	public long getIdUsuario() {
		return idPelicula;
	}
	public void setIdUsuario(long idUsuario) {
		this.idPelicula = idUsuario;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getNumeroRatings() {
		return numeroRatings;
	}
	public void setNumeroRatings(int numeroRatings) {
		this.numeroRatings = numeroRatings;
	}
	public int getNumeroTags() {
		return numeroTags;
	}
	public void setNumeroTags(int numeroTags) {
		this.numeroTags = numeroTags;
	}
	public double getPromedioRatings() {
		return promedioRatings;
	}
	public void setPromedioRatings(double promedioRatings) {
		this.promedioRatings = promedioRatings;
	}
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	public ILista<VOTag> getTagsAsociados() {
		return tagsAsociados;
	}
	public void setTagsAsociados(ILista<VOTag> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	public double getDiferencia(){
		return diferencia;
	}
	public void setDiferencia(double dif){
		diferencia=dif;
	}
	@Override
	public int compareTo(VOPelicula o) 
	{
		if( idPelicula == o.getIdPelicula() )
		{
			return 0;
		}
		else if ( idPelicula > o.getIdPelicula() )
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
	
	public ComparatorbyNumeroTags compardorNumeroTags(){
		return new ComparatorbyNumeroTags();
	}
	
	public static class ComparatorbyNumeroTags implements Comparator<VOPelicula>
	{
		public int compare(VOPelicula t1, VOPelicula t2) 
		{
			if(t1.getNumeroTags() > t2.getNumeroTags() )
			{
				return 1;
			}
			else if( t1.getNumeroTags() < t2.getNumeroTags() )
			{
				return -1;
			}
			else
			{
				return 0;
			}
		}
		
	}
	public String toString(){
		return titulo+agnoPublicacion;
	}
	

}
