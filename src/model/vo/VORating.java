package model.vo;

public class VORating implements Comparable<VORating>{

	private long idUsuario;
	private long idPelicula;
	private double rating;
	private long timestamp;

	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public void setTimestamp( long timestamp )
	{
		this.timestamp = timestamp;
	}
	public long getTimestamp( )
	{
		return timestamp;
	}
	@Override
	public int compareTo(VORating o) 
	{
		if( idPelicula == o.getIdPelicula() )
		{
			return 0;
		}
		else if( idPelicula > o.getIdPelicula() )
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}

}
