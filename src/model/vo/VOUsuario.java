package model.vo;

import model.data_structures.Comparator;

public class VOUsuario implements Comparable<VOUsuario>
{

	private long idUsuario;	
	private long primerTimestamp;
	private int numRatings;
	private double diferenciaOpinion;
	
	public long getIdUsuario() {
 		return idUsuario;
 	}
	public void setIdUsuario(long id) {
		this.idUsuario = id;
 	}
 	public long getPrimerTimestamp() {
 		return primerTimestamp;

	}
	public void setPrimerTimestamp(long primerTimestamp) {
		this.primerTimestamp = primerTimestamp;
	}
	public int getNumRatings() {
		return numRatings;
	}
	public void setNumRatings(int numRatings) {
		this.numRatings = numRatings;
	}
	public double getDiferenciaOpinion() {
		return diferenciaOpinion;
	}
	public void setDiferenciaOpinion(double diferenciaOpinion) {
		this.diferenciaOpinion = diferenciaOpinion;
	}
	@Override
	public int compareTo(VOUsuario o) 
	{
		if( idUsuario == o.getIdUsuario() )
		{
			return 0;
		}
		else if( idUsuario > o.getIdUsuario() )
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
	
	public ComparatorbyPrimerTimestamp compardorPrimerTimestamp()
	{
		return new ComparatorbyPrimerTimestamp();
	}
	
	public static class ComparatorbyPrimerTimestamp implements Comparator<VOUsuario>
	{
		public int compare(VOUsuario t1, VOUsuario t2) 
		{
			if( t1.getPrimerTimestamp() > t2.getPrimerTimestamp() )
			{
				return 1;
			}
			else if( t1.getPrimerTimestamp() < t2.getPrimerTimestamp() )
			{
				return -1;
			}
			else
			{
				return 0;
			}
		}
		
	}
	
	public ComparatorbyNumRatings compardorNumRatings()
	{
		return new ComparatorbyNumRatings();
	}
	
	public static class ComparatorbyNumRatings implements Comparator<VOUsuario>
	{
		public int compare(VOUsuario t1, VOUsuario t2) 
		{
			if( t1.getNumRatings() > t2.getNumRatings() )
			{
				return 1;
			}
			else if( t1.getNumRatings() < t2.getNumRatings() )
			{
				return -1;
			}
			else
			{
				return 0;
			}
		}
		
	}
	
	public ComparatorbyID compardorID()
	{
		return new ComparatorbyID();
	}
	
	public static class ComparatorbyID implements Comparator<VOUsuario>
	{
		public int compare(VOUsuario t1, VOUsuario t2) 
		{
			if( t1.getIdUsuario() > t2.getIdUsuario() )
			{
				return 1;
			}
			else if( t1.getIdUsuario() < t2.getIdUsuario() )
			{
				return -1;
			}
			else
			{
				return 0;
			}
		}
		
	}
}
