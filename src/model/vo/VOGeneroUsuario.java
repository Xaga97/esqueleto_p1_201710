package model.vo;

import model.data_structures.Comparator;
import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.data_structures.elementoAux;
import model.vo.VOUsuarioConteo.ComparatorbyConteo;

public class VOGeneroUsuario implements Comparable<VOGeneroUsuario>
{

	private String genero;
	private ILista<VOUsuarioConteo> usuarios;
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public ILista<VOUsuarioConteo> getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(ILista<VOUsuarioConteo> usuarios) {
		this.usuarios = usuarios;
	}
	@Override
	public int compareTo(VOGeneroUsuario o) 
	{
		return genero.compareTo( o.getGenero() );
	}


}
