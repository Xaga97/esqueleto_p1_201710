package model.logic;


import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.data_structures.Queue;
import model.data_structures.elementoAux;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroTag;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPelicula.ComparatorbyNumeroTags;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOTag.ComparatorbyMasReciente;
import model.vo.VOTag.ComparatorbyNombre;
import model.vo.VOUsuario;
import model.vo.VOUsuario.ComparatorbyID;
import model.vo.VOUsuario.ComparatorbyNumRatings;
import model.vo.VOUsuario.ComparatorbyPrimerTimestamp;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioConteo.ComparatorbyConteo;
import model.vo.VOUsuarioGenero;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import api.ISistemaRecomendacionPeliculas;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas{
	ListaDobleEncadenada<VOPelicula> peliculas;
	ListaEncadenada<VOGeneroPelicula> genpeliculas;
	ListaDobleEncadenada<VORating> ratings;
	ListaEncadenada<VOTag> tags;
	ListaEncadenada<VOGeneroTag> gentags;
	ListaEncadenada<VOUsuario> usuarios;
	ListaEncadenada<VOGeneroUsuario> genusuarios;
	boolean genusuarios_ordenado;
	ListaEncadenada<VOOperacion> operaciones;
	ListaEncadenada<VOUsuarioGenero> usuariosgenero;
	
	String rutaRecomendacion;
	
	public SistemaRecomendacionPeliculas( )
	{
		
	}
	
	public void inicializar( )
	{
		peliculas = new ListaDobleEncadenada<VOPelicula>();
		genpeliculas = new ListaEncadenada<VOGeneroPelicula>();
		ratings = new ListaDobleEncadenada<VORating>();
		tags = new ListaEncadenada<VOTag>();
		gentags = new ListaEncadenada<VOGeneroTag>();
		usuarios = new ListaEncadenada<VOUsuario>();
		genusuarios = new ListaEncadenada<VOGeneroUsuario>();
		operaciones = new ListaEncadenada<VOOperacion>();
		usuariosgenero = new ListaEncadenada<VOUsuarioGenero>();
	}

	
//	public boolean cargarPeliculasSR(String Ruta) 
//	{
//		boolean cargo = false;
//		genpeliculas = new ListaEncadenada<VOGeneroPelicula>();
//
//		peliculas = new ListaDobleEncadenada<VOPelicula>(); 
//		try
//		{
//			BufferedReader br = new BufferedReader(new FileReader(Ruta));
//
//			String line = br.readLine();
//			VOPelicula temp;
//			String data;
//			String[] dataSplit;
//			Long idUsuario;
//			while(line!= null)
//			{
//
//				temp = new VOPelicula();
//				data = line;
//				temp.setAgnoPublicacion(sacarAgno(data));
//				dataSplit = data.split(","); 
//
//				if(isNumber(dataSplit[0]))
//				{
//					idUsuario = Long.parseLong(dataSplit[0].replaceAll("\"",""));
//					temp.setIdUsuario(idUsuario);
//					temp.setTitulo(sacarNombre(dataSplit));
//					temp.setGenerosAsociados(sacarGeneros(dataSplit));
//					peliculas.agregarElementoFinal(temp);
//				}
//				line = br.readLine();
//
//			}
//			if(peliculas.darNumeroElementos() >0)
//			{
//				peliculasPorGenero();
//			}
//			
//			br.close();
//			cargo = true;
//
//		}
//		catch(IOException e){
//			e.printStackTrace();
//		
//		}
//		return cargo;
//		
//	}
	
	private void peliculasPorGenero()
	{
		ILista<VOGeneroPelicula> peliculasPorGenero = new ListaEncadenada<VOGeneroPelicula>();
		
		peliculas.cambiarActualPrimero();
		VOPelicula peliTempo;
		VOGeneroPelicula generoTempo;
		boolean generoExiste;
		ListaEncadenada<VOPelicula> listaPelicula;
		String generoPeli;

		//Asigna los generos
		for(int i =0; i < peliculas.darNumeroElementos(); i++)
		{
			generoExiste =false;
			generoTempo = new VOGeneroPelicula();
			peliTempo = peliculas.darElemento(i);
			
			if(peliculasPorGenero.darNumeroElementos() ==0)
			{
				generoTempo.setGenero(peliTempo.getGenerosAsociados().darElemento(0));
				peliculasPorGenero.agregarElementoFinal(generoTempo);
			}
			else
			{
				for(int j =0; j <= peliculasPorGenero.darNumeroElementos() && !generoExiste; j++)
				{

					generoTempo = peliculasPorGenero.darElemento(j);
					generoPeli = peliTempo.getGenerosAsociados().darElemento(0);
					if(generoPeli.equals(generoTempo.getGenero())){
						generoExiste = true;
					}

				}
				
				if(!generoExiste)
				{
					generoTempo = new VOGeneroPelicula();
					generoTempo.setGenero(peliTempo.getGenerosAsociados().darElemento(0));
					peliculasPorGenero.agregarElementoFinal(generoTempo);

				}
			}
		}
		
		String genero;
		peliculasPorGenero.cambiarActualPrimero();
		
		//Asigna las peliculas a cada genero
		for(int i =0; i < peliculasPorGenero.darNumeroElementos(); i++)
		{
			peliculas.cambiarActualPrimero();
			
			generoTempo = peliculasPorGenero.darElemento(i);
			genero = generoTempo.getGenero();
			listaPelicula = new ListaEncadenada<VOPelicula>();
			for(int j =0; j < peliculas.darNumeroElementos(); j++)
			{
				
				peliTempo = peliculas.darElemento(j);
				if(genero.equals(peliTempo.getGenerosAsociados().darElemento(0)))
				{
					listaPelicula.agregarElementoFinal(peliTempo);
				}
			}
			generoTempo.setPeliculas(listaPelicula);
		}
		
		genpeliculas = (ListaEncadenada<VOGeneroPelicula>) peliculasPorGenero;
		
	}
	
	private ILista<String> sacarGeneros(String[] dataSplit){
		ILista<String> generos = new ListaEncadenada<String>();

		String[] generosSplit = dataSplit[dataSplit.length-1].split("[|]");
		for(String tempo: generosSplit){
			generos.agregarElementoFinal(tempo.replaceAll("\"", ""));
		}

		return generos;
	}

	private String sacarNombre(String[] dataSplit){
		String nombre1 = "";
		String nombre = "";
		for(int i = 1; i < dataSplit.length-1; i++){

			nombre1 =  dataSplit[i];
			nombre += nombre1.replaceAll("\"", "");
		}

		return nombre;
	}

	private int sacarAgno(String data){

		String[] arregloAgno = data.split("[(]");
		int agno =0;

		for(int i =1; i < arregloAgno.length && agno ==0; i++){

			String temp = arregloAgno[arregloAgno.length-i].split("[)]")[0];
			if(isNumber(temp))
				agno = Integer.parseInt(temp.split("-")[0]);
		}


		return agno;

	}

	private boolean isNumber(String dato){
		try{
			Double d = Double.parseDouble(dato);
		}
		catch(NumberFormatException e){
			String agno= dato.split("-")[0];

			try{
				Double dou = Double.parseDouble(agno);

			}
			catch(NumberFormatException e2)
			{
				String partidoComilla = dato.replaceAll("\"","");
				try{
					Double dou = Double.parseDouble(partidoComilla);
				}
				catch(Exception e3){return false;}
				return true;
			}
			return true;
		}
		return true;
	}
	
	
	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) 
	{
		boolean cargo = false;
		try
		{
			CSVReader reader = new CSVReader(new FileReader(rutaPeliculas));
			String [] nextLine = reader.readNext();
			nextLine = reader.readNext();
			while(nextLine != null) 
		    {
				VOPelicula pelicula = new VOPelicula();
//				pelicula.setAgnoPublicacion(sacarAgno(data));
				
				String toStrng = Arrays.toString(nextLine);
				System.out.println(toStrng);
				toStrng.replaceAll("\\[", "").replaceAll("\\]","");
//				toStrng.replace("\\]", "");
				String regex = "\\[|\\]";
				 toStrng = toStrng.replaceAll(regex, "");
				 System.out.println(toStrng);
				String [] pieces = toStrng.split(", ");
				long idUsuario=Long.parseLong(pieces[0]);
				String titulo ="";
				int agno = 0;
				System.out.println(pieces[1]);
				if( (pieces[1]).indexOf("\\(") <= 0 )
				{
					titulo = nextLine[1];
				}
				else if( pieces[1].indexOf("\\(") >= 2 )
				{
					String [] info = (pieces[1].replace("\\[", "")).split("\\(");
					titulo = info[0];
					
					agno = Integer.parseInt(info[2].replaceAll("\\)", ""));
				}
				else
				{
					String [] info = (pieces[1].replaceAll("\\[", "")).split("\\(");
					titulo = info[0];
					
					agno = Integer.parseInt(info[1].replaceAll("\\)", ""));
				}
				
	    	
	    	System.out.println(sacarNombre(pieces));
	    	System.out.println("cargó");
    		
    		nextLine = reader.readNext();
    		
    		ILista<VOTag> tagsAsociados = new ListaEncadenada<VOTag>();
    		for( int i = 0; i < tags.darNumeroElementos(); i++ )
    		{
    			if( tags.darElemento(i).getIDPelicula() == idUsuario )
    			{
    				tagsAsociados.agregarElementoFinal(tags.darElemento(i));
    			}
    		}
    		pelicula.setTagsAsociados(tagsAsociados);
	    	peliculas.agregarElementoFinal(pelicula);
	    	
	    	ILista<String> generosAsociados = new ListaEncadenada<String>();

	    	String generos= Arrays.toString(pieces[2].split("\\|"));
	    	generos = generos.replaceAll(regex, "");
	    	String [] generosFinal = generos.split(", ");
//		       String [] genresV = nextLine[2].split("\\|");
		       pelicula.setGenerosAsociados(generosAsociados);
		       for( int i = 0; i < generosFinal.length; i++)
		       {
		    	   pelicula.getGenerosAsociados().agregarElementoFinal(generosFinal[i]);
		    	   VOGeneroPelicula genero = new VOGeneroPelicula();
		    	   ListaEncadenada<VOPelicula> peliculasGenero = new ListaEncadenada<VOPelicula>();
		    	   genero.setPeliculas(peliculasGenero);
		    	   genero.setGenero(generosFinal[i]);
		    	   genero.getPeliculas().agregarElementoFinal(pelicula);
		    	   if( genpeliculas.buscarElemento(genero) == null)
		    	   {
		    		   genpeliculas.agregarElementoFinal(genero);
		    	   }
		       }
		       
		       for( int i = 0; i < generosFinal.length; i++)
		       {
		    	   VOGeneroTag genTag = new VOGeneroTag();
		    	   genTag.setGenero(generosFinal[i]);
		    	   VOGeneroPelicula buscar = new VOGeneroPelicula();
	    		   buscar.setGenero(generosFinal[i]);
	    		   VOGeneroPelicula buscado = genpeliculas.buscarElemento(buscar);
	    		   if( buscado != null )
	    		   {
	    			   for( int j = 0; j < buscado.getPeliculas().darNumeroElementos(); j++ )
		    		   {
		    			   for( int k = 0; k < buscado.getPeliculas().darElemento(j).getTagsAsociados().darNumeroElementos(); k++ )
		    			   {
		    				   VOTag agregar = new VOTag();
		    				   agregar.setTag(buscado.getPeliculas().darElemento(j).getTagsAsociados().darElemento(k).getTag());
		    				   genTag.getTags().agregarElementoFinal( agregar );
		    			   }
		    		   }
			    	   if( gentags.buscarElemento(genTag) == null)
			    	   {   
			    		   gentags.agregarElementoFinal(genTag);
			    	   }
	    		   }
		       }
	
	    	nextLine = reader.readNext();
			
		}
			reader.close();
			cargo = true;
		
		
//		    while ((nextLine = reader.readNext()) != null) 
//		    {
//		    	VOPelicula pelicula = new VOPelicula();
//		    	String toStrng = Arrays.toString(nextLine);
//		    	String [] pieces = toStrng.split(", ");
//		    	long id = Long.parseLong(pieces[0].replace("[", ""));
//		    	pelicula.setIdPelicula(id);
//		    	String titulo ="";
//		    	int agno = 0;
//		    	System.out.println(pieces[1]);
//		    	if( (pieces[1]).indexOf("(") <= 0 )
//		    	{
//		    		titulo = nextLine[1];
//		    	}
//		    	else
//		    	{
//		    		String [] info = (pieces[1].replace("[", "")).split("\\(");
//				    titulo = info[0];
//				    agno = Integer.parseInt(info[1].replace(")", ""));
//		    	}
//		    	
//		    	System.out.println(titulo);
//		    	pelicula.setAgnoPublicacion(agno);
//		    	System.out.println("cargó");
//	    		pelicula.setTitulo(titulo);
//	    		
//			
//		    	
		}
		catch( FileNotFoundException e)
		{
			System.out.println("No se encontró el archivo: " + e.getStackTrace());
		} 
		catch (IOException e) 
		{
			System.out.println("Error en la lectura del archivo" + e.getStackTrace());
		}
		return cargo;
	}

	@Override
	public boolean cargarRatingsSR(String rutaRatings) 
	{
		boolean cargo = false;
		try
		{
			VORating rating = new VORating();
			CSVReader reader = new CSVReader(new FileReader(rutaRatings));
			String [] nextLine = reader.readNext();
		    while ((nextLine = reader.readNext()) != null)
		    {
		    	rating.setIdUsuario(Long.parseLong(nextLine[0]));
		    	rating.setIdPelicula(Long.parseLong(nextLine[1]));
		    	rating.setRating(Double.parseDouble(nextLine[2]));
		    	rating.setTimestamp(Long.parseLong(nextLine[3]));
		    	
		    	VOUsuario usuario = new VOUsuario();
		    	usuario.setIdUsuario(Long.parseLong(nextLine[0]));
		    	if( usuarios.buscarElemento(usuario) == null )
		    	{
		    		usuario.setPrimerTimestamp(Long.parseLong(nextLine[3]));
		    		usuarios.agregarElementoFinal(usuario);
		    	}
		    	
		    }
		    ratings.agregarElementoFinal(rating);
		   		    
		    cargo = true;
		}
		catch( FileNotFoundException e)
		{
			System.out.println("No se encontró el archivo: " + e.getStackTrace());
		} 
		catch (IOException e) 
		{
			System.out.println("Error en la lectura del archivo" + e.getStackTrace());
		}
		return cargo;
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) 
	{
		boolean cargo = false;
		try
		{
			VOTag tag = new VOTag();
			CSVReader reader = new CSVReader(new FileReader(rutaTags));
			String [] nextLine = reader.readNext();
		    while ((nextLine = reader.readNext()) != null)
		    {
		    	String toStrng = Arrays.toString(nextLine);
		    	String [] pieces = toStrng.split(", ");
		    	tag.setIDUsuario(Long.parseLong((pieces[0].replace("[", ""))));
		    	tag.setIDPelicula(Long.parseLong((pieces[1])));
		    	tag.setTag(pieces[2]);
		    	System.out.println(Long.parseLong(pieces[3].replace("]", "")));
		    	tag.setTimestamp(Long.parseLong(pieces[3].replace("]", "")));

			    tags.agregarElementoFinal(tag);
		    }
		    
		    VOPelicula buscada = new VOPelicula();
		    buscada.setIdPelicula( tag.getIDPelicula() );
		    if( peliculas.buscarElemento( buscada ) != null )
		    {
		    	buscada.getTagsAsociados().agregarElementoFinal(tag);
		    }
		    else
		    {
		    	peliculas.agregarElementoFinal(buscada);
		    	buscada.getTagsAsociados().agregarElementoFinal(tag);
		    }
		    
		    
		    
		    cargo = true;
		}
		catch( FileNotFoundException e)
		{
			System.out.println("No se encontró el archivo: " + e.getStackTrace());
		} 
		catch (IOException e) 
		{
			System.out.println("Error en la lectura del archivo" + e.getStackTrace());
		}
		return cargo;
	}
	
	public void cargarUsuariosGenero( )
	{
		for( int i = 0; i < usuarios.darNumeroElementos(); i++ )
		{
			VOUsuarioGenero usuario = new VOUsuarioGenero();
			usuario.setIdUsuario(usuarios.darElemento(i).getIdUsuario());
			ILista<VOGeneroTag> gentagsAgregar = new ListaEncadenada<VOGeneroTag>();
			usuario.setListaGeneroTags(gentagsAgregar);
			for( int j = 0; j < gentags.darNumeroElementos(); j++ )
			{
				VOGeneroTag gentag = new VOGeneroTag();
				gentag.setGenero(gentags.darElemento(j).getGenero());
				gentagsAgregar.agregarElementoFinal(gentag);
				ILista<VOTag> tagsAgregar = new ListaEncadenada<VOTag>();
				gentag.setTags(tagsAgregar);
				for( int k = 0; k < gentags.darElemento(j).getTags().darNumeroElementos(); k++ )
				{
					if( gentags.darElemento(j).getTags().darElemento(k).getIDUsuario() == usuario.getIdUsuario() )
					{
						tagsAgregar.agregarElementoFinal(gentags.darElemento(j).getTags().darElemento(k));
					}
				}
				
			}
		}
	}

	@Override
	public int sizeMoviesSR() {
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Size movies SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		// TODO Auto-generated method stub
		return peliculas.darNumeroElementos();
	}

	@Override
	public int sizeUsersSR() {
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Size users SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		// TODO Auto-generated method stub
		return usuarios.darNumeroElementos();
	}

	@Override
	public int sizeTagsSR() {
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Size tags SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		// TODO Auto-generated method stub
		return tags.darNumeroElementos();
	}

	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		cargarPromedioRatings();
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Peliculas Populares SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
		ListaEncadenada<VOGeneroPelicula> s = new ListaEncadenada<VOGeneroPelicula>();
		crearListasGenero();
		for(VOGeneroPelicula t : genpeliculas){
			VOPelicula[]m= new VOPelicula[t.getPeliculas().darNumeroElementos()];
			for(int i =0;i<t.getPeliculas().darNumeroElementos();i++){
				m[i]=t.getPeliculas().darElemento(i);
			}
			VOPelicula auxiliar[] = new VOPelicula[t.getPeliculas().darNumeroElementos()];
			for(int v = 2; v/2<t.getPeliculas().darNumeroElementos();v*=2){
				for(int posi=0;posi<t.getPeliculas().darNumeroElementos() && posi+v/2 < t.getPeliculas().darNumeroElementos();posi+=v){
					int posm=posi+v/2;
					int posf=Math.min(posi+v,t.getPeliculas().darNumeroElementos());
					for(int i = posi, d = posm, pi=posi;i<posm||d<posf;){
						if(i==posm){
							auxiliar[pi++]=m[d++];
						}
						else if(d==posf){
							auxiliar[pi++]= m[i++];
						}else{
							if(m[i].getPromedioRatings()<m[d].getPromedioRatings()){
								auxiliar[pi++]=m[i++];
							}
							else{
								auxiliar[pi++]=m[d++];
							}
						}
					}
					for(int p = posi;p<posf;p++){
						m[p]=auxiliar[p];
					}	    	
				}
			}
			t.getPeliculas().clear();
			for(int f=0;f<m.length;f++){
				t.getPeliculas().agregarElementoFinal(m[f]);
			}
			VOGeneroPelicula y=new VOGeneroPelicula();
			y.setGenero(t.getGenero());
			ListaEncadenada<VOPelicula>x=new ListaEncadenada<VOPelicula>();
			for(int i = 0; i < n; i ++){
				x.agregarElementoFinal(t.getPeliculas().darElemento(i));
			}
			y.setPeliculas(x);
			s.agregarElementoFinal(y);
		}
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		return s;
	}

	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		// TODO Auto-generated method stub
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Catalogo Peliculas Ordenado SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
		VOPelicula[]m= new VOPelicula[peliculas.darNumeroElementos()];
		for(int i =0;i<peliculas.darNumeroElementos();i++){
			m[i]=peliculas.darElemento(i);
		}
		VOPelicula auxiliar[] = new VOPelicula[peliculas.darNumeroElementos()];
		for(int v = 2; v/2<peliculas.darNumeroElementos();v*=2){
			for(int posi=0;posi<peliculas.darNumeroElementos() && posi+v/2 < peliculas.darNumeroElementos();posi+=v){
				int posm=posi+v/2;
				int posf=Math.min(posi+v, peliculas.darNumeroElementos());
				for(int i = posi, d = posm, pi=posi;i<posm||d<posf;){
					if(i==posm){
						auxiliar[pi++]=m[d++];
					}
					else if(d==posf){
						auxiliar[pi++]= m[i++];
					}else{
						if(m[i].getAgnoPublicacion()-m[d].getAgnoPublicacion()<0){
							auxiliar[pi++]=m[i++];
						}
						else if(m[i].getAgnoPublicacion()-m[d].getAgnoPublicacion()==0){
							if(m[i].getPromedioRatings()-m[d].getPromedioRatings()<0){
								auxiliar[pi++]=m[i++];
							}
							else if(m[i].getAgnoPublicacion()-m[d].getAgnoPublicacion()==0){
								if(m[i].getTitulo().compareTo(m[d].getTitulo())<0){
									auxiliar[pi++]=m[i++];
								}
							}
						}
						else auxiliar[pi++]=m[d++];

					}
				}
				for(int s = posi;s<posf;s++){
					m[s]=auxiliar[s];
				}	    	
			}
		}
		peliculas.clear();
		for(int f=0;f<m.length;f++){
			peliculas.agregarElementoFinal(m[f]);
		}
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		return null;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		crearListasGenero();
		cargarPromedioRatings();
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Recomendar Genero SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
		ListaEncadenada<VOGeneroPelicula> s = new ListaEncadenada<VOGeneroPelicula>();
		for(VOGeneroPelicula t : genpeliculas){
			VOPelicula[]m= new VOPelicula[t.getPeliculas().darNumeroElementos()];
			for(int i =0;i<t.getPeliculas().darNumeroElementos();i++){
				m[i]=t.getPeliculas().darElemento(i);
			}
			VOPelicula auxiliar[] = new VOPelicula[t.getPeliculas().darNumeroElementos()];
			for(int v = 2; v/2<t.getPeliculas().darNumeroElementos();v*=2){
				for(int posi=0;posi<t.getPeliculas().darNumeroElementos() && posi+v/2 < t.getPeliculas().darNumeroElementos();posi+=v){
					int posm=posi+v/2;
					int posf=Math.min(posi+v,t.getPeliculas().darNumeroElementos());
					for(int i = posi, d = posm, pi=posi;i<posm||d<posf;){
						if(i==posm){
							auxiliar[pi++]=m[d++];
						}
						else if(d==posf){
							auxiliar[pi++]= m[i++];
						}else{
							if(m[i].getPromedioRatings()<m[d].getPromedioRatings()){
								auxiliar[pi++]=m[i++];
							}
							else{
								auxiliar[pi++]=m[d++];
							}
						}
					}
					for(int p = posi;p<posf;p++){
						m[p]=auxiliar[p];
					}	    	
				}
			}
			t.getPeliculas().clear();
			for(int f=0;f<m.length;f++){
				t.getPeliculas().agregarElementoFinal(m[f]);
			}
			VOGeneroPelicula y=new VOGeneroPelicula();
			y.setGenero(t.getGenero());
			ListaEncadenada<VOPelicula>x=new ListaEncadenada<VOPelicula>();
			x.agregarElementoFinal(t.getPeliculas().darElemento(0));
			y.setPeliculas(x);
			s.agregarElementoFinal(y);
		}
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		return s;

	}

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		// TODO Auto-generated method stub
		crearListasGenero();
		cargarPromedioRatings();
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Opinion Ratings Genero SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
		ListaEncadenada<VOGeneroPelicula> s = new ListaEncadenada<VOGeneroPelicula>();
		for(VOGeneroPelicula t : genpeliculas){
			VOPelicula[]m= new VOPelicula[t.getPeliculas().darNumeroElementos()];
			for(int i =0;i<t.getPeliculas().darNumeroElementos();i++){
				m[i]=t.getPeliculas().darElemento(i);
			}
			VOPelicula auxiliar[] = new VOPelicula[t.getPeliculas().darNumeroElementos()];
			for(int v = 2; v/2<t.getPeliculas().darNumeroElementos();v*=2){
				for(int posi=0;posi<t.getPeliculas().darNumeroElementos() && posi+v/2 < t.getPeliculas().darNumeroElementos();posi+=v){
					int posm=posi+v/2;
					int posf=Math.min(posi+v,t.getPeliculas().darNumeroElementos());
					for(int i = posi, d = posm, pi=posi;i<posm||d<posf;){
						if(i==posm){
							auxiliar[pi++]=m[d++];
						}
						else if(d==posf){
							auxiliar[pi++]= m[i++];
						}else{
							if(m[i].getAgnoPublicacion()<m[d].getAgnoPublicacion()){
								auxiliar[pi++]=m[i++];
							}
							else{
								auxiliar[pi++]=m[d++];
							}
						}
					}
					for(int p = posi;p<posf;p++){
						m[p]=auxiliar[p];
					}	    	
				}
			}
			t.getPeliculas().clear();
			for(int f=0;f<m.length;f++){
				t.getPeliculas().agregarElementoFinal(m[f]);
			}
			VOGeneroPelicula y=new VOGeneroPelicula();
			y.setGenero(t.getGenero());
			ListaEncadenada<VOPelicula>x=new ListaEncadenada<VOPelicula>();
			for(int i = 0; i < t.getPeliculas().darNumeroElementos(); i ++){
				x.agregarElementoFinal(t.getPeliculas().darElemento(i));
			}
			y.setPeliculas(x);
			s.agregarElementoFinal(y);
		}
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		return s;
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(
			String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		cargarPromedioRatings();
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Recomendar Peliculas SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
		try
		{
			VOPelicula peli = new VOPelicula();
			ListaEncadenada<VOPeliculaPelicula>q=new ListaEncadenada<VOPeliculaPelicula>();
			Queue<VOPelicula> x = new Queue<VOPelicula>();
			CSVReader reader = new CSVReader(new FileReader(rutaRecomendacion));
			String [] nextLine = reader.readNext();
			while ((nextLine = reader.readNext()) != null)
			{
				peli.setIdPelicula(Long.parseLong(nextLine[0]));
				peli.setPromedioRatings(Double.parseDouble(nextLine[1]));
				x.enqueue(peli);
			}
			while(!x.isEmpty()){
				ListaEncadenada<VOPelicula>t=new ListaEncadenada<VOPelicula>();
				VOPeliculaPelicula i = new VOPeliculaPelicula();
				double rat = x.darPrimero().getPromedioRatings();
				long id = x.dequeue().getIdPelicula();
				boolean j = false;
				VOPelicula u = new VOPelicula();
				for(int p = 0; p < peliculas.darNumeroElementos()&&j==false;p++)if(peliculas.darElemento(p).getIdPelicula()==id){
					u=peliculas.darElemento(p);
					j=true;
				}
				u.setPromedioRatings(rat);
				i.setPelicula(u);
				String princ = u.getGenerosAsociados().darElemento(0);
				for(VOPelicula o:peliculas){
					j=false;
					for(int l = 0; l<o.getGenerosAsociados().darNumeroElementos()&&j==false;l++){
						String r = o.getGenerosAsociados().darElemento(l);
						if(r.equals(princ)){
							j=true;
						}
					}
					if(j==true){
						if(o.getPromedioRatings()-u.getPromedioRatings()<0.5&&o.getPromedioRatings()-u.getPromedioRatings()>0.5){
							o.setDiferencia(o.getPromedioRatings()-u.getPromedioRatings());
							t.agregarElementoFinal(o);
						}
					}
				}
				VOPelicula[]m= new VOPelicula[t.darNumeroElementos()];
				for(int k =0;k<t.darNumeroElementos();k++){
					m[k]=t.darElemento(k);
				}
				VOPelicula auxiliar[] = new VOPelicula[t.darNumeroElementos()];
				for(int v = 2; v/2<t.darNumeroElementos();v*=2){
					for(int posi=0;posi<t.darNumeroElementos() && posi+v/2 < t.darNumeroElementos();posi+=v){
						int posm=posi+v/2;
						int posf=Math.min(posi+v, t.darNumeroElementos());
						for(int h = posi, d = posm, pi=posi;h<posm||d<posf;){
							if(h==posm){
								auxiliar[pi++]=m[d++];
							}
							else if(d==posf){
								auxiliar[pi++]= m[h++];
							}else{
								if(m[h].getDiferencia()-(m[d].getDiferencia())<0){
									auxiliar[pi++]=m[h++];
								}
								else{
									auxiliar[pi++]=m[d++];
								}
							}
						}
						for(int s = posi;s<posf;s++){
							m[s]=auxiliar[s];
						}	    	
					}
				}
				t.clear();
				for(int f=0;f<m.length;f++){
					t.agregarElementoFinal(m[f]);
				}
				i.setPeliculasRelacionadas(t);
				q.agregarElementoFinal(i);
			}
			return q;
		}
		catch( FileNotFoundException e)
		{
			System.out.println("No se encontró el archivo: " + e.getStackTrace());
		} 
		catch (IOException e) 
		{
			System.out.println("Error en la lectura del archivo" + e.getStackTrace());
		}
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		return null;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		// TODO Auto-generated method stub
		cargarPromedioRatings();
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Ratings Pelicula SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
		ListaEncadenada <VORating> esta = new ListaEncadenada<VORating>();
		for(VORating m:ratings){
			if(m.getIdPelicula()==idPelicula)esta.agregarElementoFinal(m);
		}
		VORating[]m= new VORating[esta.darNumeroElementos()];
		for(int i =0;i<esta.darNumeroElementos();i++){
			m[i]=esta.darElemento(i);
		}
		VORating auxiliar[] = new VORating[esta.darNumeroElementos()];
		for(int v = 2; v/2<esta.darNumeroElementos();v*=2){
			for(int posi=0;posi<esta.darNumeroElementos() && posi+v/2 < esta.darNumeroElementos();posi+=v){
				int posm=posi+v/2;
				int posf=Math.min(posi+v, esta.darNumeroElementos());
				for(int i = posi, d = posm, pi=posi;i<posm||d<posf;){
					if(i==posm){
						auxiliar[pi++]=m[d++];
					}
					else if(d==posf){
						auxiliar[pi++]= m[i++];
					}else{
						if(m[i].compareTo(m[d])<0){
							auxiliar[pi++]=m[i++];
						}
						else{
							auxiliar[pi++]=m[d++];
						}
					}
				}
				for(int s = posi;s<posf;s++){
					m[s]=auxiliar[s];
				}	    	
			}
		}
		esta.clear();
		for(int f=0;f<m.length;f++){
			esta.agregarElementoFinal(m[f]);
		}
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		return esta;
	}

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) 
	{
		// TODO Auto-generated method stub
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Usuarios activos SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
		
		cargarGenUsuarios();
		if( !genusuarios_ordenado )
		{
			for( int i = 0; i < genusuarios.darNumeroElementos(); i++ )
			{
				VOUsuarioConteo usuario = new VOUsuarioConteo();
				ComparatorbyConteo comp = usuario.compardorConteo();
				genusuarios.darElemento(i).getUsuarios().sort(genusuarios.darElemento(i).getUsuarios() , comp, false);
				genusuarios_ordenado = true;
			}
		}
		
		ILista<VOGeneroUsuario> generos_MasActivos = new ListaEncadenada<VOGeneroUsuario>();
		for( int i = 0; i < genusuarios.darNumeroElementos(); i++ )
		{
			VOGeneroUsuario gen = new VOGeneroUsuario();
			gen.setGenero(genusuarios.darElemento(i).getGenero());
			ILista<VOUsuarioConteo> masActivos = new ListaEncadenada<VOUsuarioConteo>();
			for( int j = 0; j < n; j++ )
			{
				masActivos.agregarElementoFinal(genusuarios.darElemento(i).getUsuarios().darElemento(j));
			}
			gen.setUsuarios(masActivos);
		}
	
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		return generos_MasActivos;
	}

	
	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() 
	{
		// TODO Auto-generated method stub
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Catalogo Usuarios Ordenado SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
		
		VOUsuario usuarioCOMP = new VOUsuario();
		ComparatorbyPrimerTimestamp compPrimerCriterio = usuarioCOMP.compardorPrimerTimestamp();
		ComparatorbyNumRatings compSegundoCriterio = usuarioCOMP.compardorNumRatings();
		ComparatorbyID compTercerCriterio = usuarioCOMP.compardorID();
		
		usuarios.sort3Criterios(usuarios, compPrimerCriterio, compSegundoCriterio, compTercerCriterio, true);
		
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		return usuarios;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		// TODO Auto-generated method stub
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Recomendar Tags Genero SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
		
		//Se ordena la lista de cada elemento de genpeliculas por el número
		//de tags descendentemente
		for( int i = 0; i < genpeliculas.darNumeroElementos(); i++ )
		{
			ILista<VOPelicula> peliculasGenero = genpeliculas.darElemento(i).getPeliculas();
			VOPelicula pelicula = new VOPelicula();
			ComparatorbyNumeroTags comp = pelicula.compardorNumeroTags();
			peliculasGenero.sort(peliculasGenero, comp, false);
		}
		
		ILista<VOGeneroPelicula> generosMasTags = new ListaEncadenada<VOGeneroPelicula>();
		for( int i = 0; i < genpeliculas.darNumeroElementos(); i++ )
		{
			VOGeneroPelicula agregar = new VOGeneroPelicula();
			agregar.setGenero(genpeliculas.darElemento(i).getGenero());
			for( int j = 0; j < n; j++ )
			{
				agregar.getPeliculas().agregarElementoFinal(genpeliculas.darElemento(i).getPeliculas().darElemento(j));
			}
			generosMasTags.agregarElementoFinal(agregar);
		}
		
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		return generosMasTags;
	}
;
	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() 
	{
		// TODO Auto-generated method stub
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Opinion Tags Genero SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
		
		for( int i = 0; i < usuariosgenero.darNumeroElementos(); i++ )
		{
			ILista<VOGeneroTag> generos = usuariosgenero.darElemento(i).getListaGeneroTags();
			for (int j = 0; j < generos.darNumeroElementos(); j++ )
			{
				ILista<VOTag> tagsOrdenar = generos.darElemento(i).getTags();
				VOTag tagComp = new VOTag();
				ComparatorbyNombre comp = tagComp.compardorNombre();
				tagsOrdenar.sort(tagsOrdenar, comp, true);
			}
		}
				
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		return null;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) 
	{
		// TODO Auto-generated method stub
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Recomendar Usuarios SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
		
		ILista<VOPeliculaUsuario> usuariosRecomendados = new ListaEncadenada<VOPeliculaUsuario>();
		try
		{
			CSVReader reader = new CSVReader(new FileReader(rutaRecomendacion));
			String [] nextLine = reader.readNext();
			Queue<VORating> colaPeliculas = new Queue<VORating>(); 
		    while ((nextLine = reader.readNext()) != null) 
		    {
		    	long id = Long.parseLong(nextLine[0]);
		    	double rating = Math.abs( Double.parseDouble(nextLine[1]) );
		    	VORating agregar = new VORating();
		    	agregar.setIdPelicula(id);
		    	agregar.setRating(rating);
		    	colaPeliculas.enqueue(agregar);
		    }
		   
		    for( int i = 0; i < colaPeliculas.size(); i++ )
		    {
		    	VORating procesar = colaPeliculas.dequeue();
		    	VOPeliculaUsuario agregarPelicula = new VOPeliculaUsuario();
		    	agregarPelicula.setIdPelicula(procesar.getIdPelicula());
		    	ILista<VOUsuario> usuariosAgregar = new ListaEncadenada<VOUsuario>();
		    	agregarPelicula.setUsuariosRecomendados(usuariosAgregar);
		    	for( int j = 0; j < n; j++ )
		    	{
		    		VORating comparar = ratings.darElemento(j);
		    		double diferencia = Math.abs( comparar.getRating() - procesar.getRating() );
		    		if( comparar.getIdPelicula() == procesar.getIdPelicula() && diferencia <= 0.5 )
		    		{
		    			VOUsuario buscar = new VOUsuario();
		    			buscar.setIdUsuario(comparar.getIdUsuario());
		    			VOUsuario usuario = usuarios.buscarElemento(buscar);
		    			usuariosAgregar.agregarElementoFinal(usuario);
		    		}
		    	}
		    	usuariosRecomendados.agregarElementoFinal(agregarPelicula);
		    	
		    }
		    
		}
		catch( FileNotFoundException e)
		{
			System.out.println("No se encontró el archivo: " + e.getStackTrace());
		} 
		catch (IOException e) 
		{
			System.out.println("Error en la lectura del archivo" + e.getStackTrace());
		}
		
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		return usuariosRecomendados;
	}

	public ILista<VOTag> tagsPeliculaSR(long idPelicula) 
	{
		// TODO Auto-generated method stub
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Tags Pelicula SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
	
		VOPelicula buscar =new VOPelicula();
		buscar.setIdPelicula(idPelicula);
		VOPelicula buscada = peliculas.buscarElemento(buscar);
		ILista<VOTag> tagsAsociados = buscada.getTagsAsociados(); 
		if( buscada != null )
		{
			VOTag paraComp = new VOTag();
			ComparatorbyMasReciente comp = paraComp.compardorMasReciente();
			tagsAsociados.sort(tagsAsociados, comp, false);
		}
		
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		return tagsAsociados;
	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		ListaEncadenada<VOOperacion> t = new ListaEncadenada<VOOperacion>();
		for(int i = operaciones.darNumeroElementos()-1;i>-1;i--)t.agregarElementoFinal(operaciones.darElemento(i));
		return t;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {
		operaciones.clear();

	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		ListaEncadenada<VOOperacion> t = new ListaEncadenada<VOOperacion>();
		for(int i = operaciones.darNumeroElementos()-1;i>n-1;i--)t.agregarElementoFinal(operaciones.darElemento(i));
		return t;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		for(int i = operaciones.darNumeroElementos()-1;i>n-1;i--)operaciones.eliminarFinal();

	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {
		long us = peliculas.darElemento(peliculas.darNumeroElementos()-1).getIdPelicula()+1;
		VOPelicula tu = new VOPelicula();
		tu.setAgnoPublicacion(agno);
		ListaEncadenada<String> gen = new ListaEncadenada<String>();
		for(int i = 0; i < generos.length;i++)gen.agregarElementoFinal(generos[i]);
		tu.setGenerosAsociados(gen);
		tu.setTitulo(titulo);
		tu.setIdPelicula(us);
		return us;
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		VORating rat = new VORating();
		rat.setIdPelicula(idPelicula);
		rat.setIdUsuario(idUsuario);
		rat.setRating(rating);
		rat.setTimestamp(System.currentTimeMillis());
		ratings.agregarElementoFinal(rat);

	}
	
	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		VOOperacion m = new VOOperacion();
		m.setOperacion(nomOperacion);
		m.setTimestampFin(tfin);
		m.setTimestampInicio(tinicio);
		operaciones.agregarElementoFinal(m);

	}
	public void crearListasGenero(){
		for(VOPelicula p : peliculas){
			for(String g : p.getGenerosAsociados()){
				boolean existe = false;
				for(int i =0;i<genpeliculas.darNumeroElementos()&&!existe;i++){
					VOGeneroPelicula ge = genpeliculas.darElemento(i);
					if(ge.getGenero().equals(g)){
						ge.getPeliculas().agregarElementoFinal(p);
						existe = true;
					}
				}
				if(!existe){
					VOGeneroPelicula ng = new VOGeneroPelicula();
					ListaEncadenada<VOPelicula> listaP = new ListaEncadenada<>();
					ng.setGenero(g);
					listaP.agregarElementoFinal(p);
					ng.setPeliculas(listaP);
					genpeliculas.agregarElementoFinal(ng);
				}
			}
		}
	}
	public void cargarPromedioRatings()
	{
		for(VOPelicula s:peliculas)
		{
			ListaEncadenada<VORating>t = new ListaEncadenada<VORating>();
			long x = s.getIdPelicula();
			for(VORating z:ratings)if(z.getIdPelicula()==x)t.agregarElementoFinal(z);
			s.setNumeroRatings(t.darNumeroElementos());
			double m = 0;
			for(VORating y:t)m+=y.getRating();
			s.setPromedioRatings(m/s.getNumeroRatings());
		}
    }

	public void cargarGenUsuarios()
	{
		for( int i = 0; i < genpeliculas.darNumeroElementos(); i++ )
		{
			VOGeneroPelicula usado = genpeliculas.darElemento(i);
		    for( int j = 0; j < usado.getPeliculas().darNumeroElementos(); j++ )
		    {
		    	VOGeneroUsuario genUsuario = new VOGeneroUsuario();
		    	genUsuario.setGenero(usado.getGenero());
		        VOUsuarioConteo agregar = new VOUsuarioConteo();
		        agregar.setIdUsuario(usado.getPeliculas().darElemento(j).getIdUsuario());
		        
		        for( int k = 0; k < ratings.darNumeroElementos(); k++ )
		        {
		        	VORating rating = new VORating();
		        	rating.setIdUsuario(agregar.getIdUsuario());
		        	if( ratings.darElemento(k).compareTo(rating) == 0 )
		        	{
		        		agregar.setConteo(agregar.getConteo() + 1);
		        	}
		        }
		        
		        if( genUsuario.getUsuarios().buscarElemento(agregar) == null )
		        {
		    	    genUsuario.getUsuarios().agregarElementoFinal(agregar);
		        }
		        
		        if( genusuarios.buscarElemento(genUsuario) == null)
				{
					  genusuarios.agregarElementoFinal(genUsuario);
				}
		    }			
		}
		
		for( int i = 0; i < genusuarios.darNumeroElementos(); i++ )
		{
			VOUsuarioConteo usuario = new VOUsuarioConteo();
			ComparatorbyConteo comp = usuario.compardorConteo();
			ILista<VOUsuarioConteo> ordenar = genusuarios.darElemento(i).getUsuarios();
			ordenar.sort( ordenar, comp, false);
			genusuarios_ordenado = true;
		}
		
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) 
	{
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Tags Pelicula SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
	
		VOPelicula buscar =new VOPelicula();
		String mid = Integer.toString(idPelicula);
		long id = Long.parseLong(mid);
		buscar.setIdPelicula(id);
		VOPelicula buscada = peliculas.buscarElemento(buscar);
		ILista<VOTag> tagsAsociados = buscada.getTagsAsociados(); 
		if( buscada != null )
		{
			VOTag paraComp = new VOTag();
			ComparatorbyMasReciente comp = paraComp.compardorMasReciente();
			tagsAsociados.sort(tagsAsociados, comp, false);
		}
		
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		return tagsAsociados;
	}

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR1(int n) 
	{
		// TODO Auto-generated method stub
		operaciones.agregarElementoFinal(new VOOperacion());
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setOperacion("Usuarios activos SR");
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampInicio(System.currentTimeMillis());
			
		if( !genusuarios_ordenado )
		{
			for( int i = 0; i < genusuarios.darNumeroElementos(); i++ )
			{
				VOUsuarioConteo usuario = new VOUsuarioConteo();
				ComparatorbyConteo comp = usuario.compardorConteo();
				genusuarios.darElemento(i).getUsuarios().sort(genusuarios.darElemento(i).getUsuarios() , comp, false);
				genusuarios_ordenado = true;
			}
		}
		
		ILista<VOGeneroUsuario> generos_MasActivos = new ListaEncadenada<VOGeneroUsuario>();
		for( int i = 0; i < genusuarios.darNumeroElementos(); i++ )
		{
			VOGeneroUsuario gen = new VOGeneroUsuario();
			gen.setGenero(genusuarios.darElemento(i).getGenero());
			ILista<VOUsuarioConteo> masActivos = new ListaEncadenada<VOUsuarioConteo>();
			for( int j = 0; j < n; j++ )
			{
				masActivos.agregarElementoFinal(genusuarios.darElemento(i).getUsuarios().darElemento(j));
			}
			gen.setUsuarios(masActivos);
		}
			
		operaciones.darElemento(operaciones.darNumeroElementos()-1).setTimestampFin(System.currentTimeMillis());
		return generos_MasActivos;
	}
	
	public void crearArchivoRecomendacion( )
	{
		try
		{
			CSVWriter writer = new CSVWriter(new FileWriter("yourfile.csv"));
		    
			for( int i = 0; i < peliculas.darNumeroElementos(); i++ )
			{
				String[] entries = new String[2];
				VOPelicula usar = peliculas.darElemento(i);
				entries[0] = Long.toString(usar.getIdPelicula());
				entries[1] = Double.toString(usar.getPromedioRatings());
			    writer.writeNext( entries );
			}
		    writer.close();

		}
		catch( IOException e )
		{
			System.out.println("Error en la escritura del archivo" + e.getStackTrace());
		}
	}
	
	public ILista<VOPelicula> getPeliculas( )
	{
		return peliculas;
	}
	
}	