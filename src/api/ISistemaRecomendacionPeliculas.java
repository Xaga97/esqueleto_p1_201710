package api;

import model.data_structures.ILista;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioGenero;

public interface ISistemaRecomendacionPeliculas {
	
	/**
	 * Carga una lista de pel�culas del archivo rutaPeliculas<br>
	 * @param rutaPeliculas ruta del archivo con listado de pel�culas 
	 * 		  en formato csv
	 * @return true si se pudo leer el archivo
	 */
	public boolean cargarPeliculasSR(String rutaPeliculas);
	
	
	/**
	 * Carga el historial de ratings del archivo rutaRatings<br>
	 * @param rutaRatings ruta del archivo con el historial de ratings 
	 * 		  en formato csv
	 * @return true si se pudo leer el archivo
	 */
	public boolean cargarRatingsSR(String rutaRatings);

	
	/**
	 * Carga una lista de tags del archivo rutaTags
	 * @param rutaTags ruta del archivo de tags en formato csv
	 * @return true si se pudo leer el archivo
	 */
	public boolean cargarTagsSR(String rutaTags);
	
	/**
	 * Retorna el n�mero de pel�culas registradas en el SR
	 * @return el n�mero de pel�culas que existen en el SR
	 */
	public int sizeMoviesSR();
	
	/**
	 * Retorna el n�mero de usuarios registrados en el SR
	 * @return el n�mero de usuarios registrados en el SR
	 */
	public int sizeUsersSR();
	
	/**
	 * Retorna el n�mero de tags registrados en el SR
	 * @return el n�mero de tags registrados en el SR
	 */
	public int sizeTagsSR();
	
	/**
	 * Retorna las n pel�culas m�s populares por cada uno de los g�neros 
	 * @param n n�mero de pel�culas populares por g�nero a retornar
	 * @return Lista donde cada posici�n tiene un g�nero y asociado a este una 
	 * 		   lista de pel�culas populares
	 */
	public ILista<VOGeneroPelicula> peliculasPopularesSR (int n);
	
	/**
	 * Retorna una lista de pel�culas ordenadas por los siguientes criterios:<br>
	 * <ol> 
	 * 	<li>A�o de publicaci�n</li>
	 * 	<li>Promedio de ratings</li>
	 * 	<li>Orden alfab�tico</li>
	 * </ol> 
	 * @return lista de pel�culas ordenada por a�o, promedio y t�tulo
	 */
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR();
	
	/**
	 * Retorna  la mejor pel�cula de cada uno de los g�neros por su rating promedio
	 * @return Lista donde cada posici�n tiene un g�nero y asociado a este tiene
	 *         una lista de tama�o 1 con la mejor pel�cula del g�nero dado su rating promedio
	 */
	public ILista<VOGeneroPelicula> recomendarGeneroSR();
	
	/**
	 * Retorna una lista de pel�culas por cada uno de los g�neros, cada una de 
	 * las pel�culas tiene sus tags asociados
	 * @return lista de pel�culas por cada g�nero, cada pel�cula tiene sus tags asociados
	 */
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR();
	
	/**
	 * Retorna una lista con las pel�culas recomendadas seg�n los ratings 
	 * recibidos en el archivo rutaRecomendacion
	 * @param rutaRecomendacion ruta del archivo con ratings a analizar para 
	 * 		  generar la recomendaci�n
	 * @param n el n�mero de pel�culas recomendadas por cada una de las 
	 *        pel�culas presentes en el archivo rutaRecomendacion
	 * @return retorna un listado de n pel�culas recomendadas por cada una de 
	 * las pel�culas presentes en el archivo rutaRecomendaci�n. Para cada 
	 * pel�cula analizada se retorna una lista de n pel�culas que tengan dentro 
	 * de sus g�neros el g�nero principal de la pel�cula analizada y que la 
	 * diferencia entre su rating promedio y el rating asignado no sea mayor 
	 * a 0.5. El listado de pel�culas debe estar ordenado de menor a mayor 
	 * diferencia absoluta entre el rating asignado por el usuario y el rating promedio 
	 */
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n);
	
	/**
	 * Retorna una lista con las calificaciones de una pel�cula determinada
	 * ordenadas por la fecha de creaci�n del rating (unix timestamp)
	 * @param idPelicula pel�cula para la cual se quieren obtener sus ratings
	 * @return lista con los ratings de la pel�cula idPelicula ordenada de 
	 *         m�s reciente a menos reciente
	 */
	public ILista<VORating> ratingsPeliculaSR (long idPelicula);
	
	/**
	 * Retorna una lista con los n usuarios m�s activos por cada uno de los g�neros
	 * presentes en las pel�culas
	 * @param n tama�o de la lista de usuarios
	 * @return una lista por cada g�nero con los n usuarios m�s activos
	 */
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n);
	
	
	/**
	 * Retorna la lista total de usuarios presentes en el sistema ordenada por los siguientes criterios<br>
	 * <ol> 
	 * <li> Fecha de primer rating realizado
	 * <li> Total de ratings
	 * <li> Id de usuario
	 * </ol>
	 * @return Lista de usuarios ordenada por los criterios enunciados
	 */
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR();
	
	/**
	 * Retorna la lista de pel�culas de tama�o n con m�s tags por cada uno de los g�neros presentes en el sistema
	 * @param n tama�o de las listas de cada g�nero
	 * @return  listado de pel�culas con m�s tags por cada g�nero
	 */
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n);
	
	/**
	 * Retorna la lista de tags por cada usuario, por cada uno de los g�neros de pel�culas para las que ha realizado tags
	 * @return lista de tags de cada uno de los g�neros, por cada usuario 
	 */
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR();
	
	/**
	 * Retorna una lista en donde en cada posici�n est� la pel�cula analizada y una lista de usuarios con opiniones similares a las registradas en el archivo rutaRecomendacion. 
	 * @param rutaRecomendacion ruta del archivo con ratings a analizar para 
	 * 		  generar la recomendaci�n
	 * @param n N�mero de usuarios recomendados por cada pel�cula
	 * @return Retorna una lista en donde en cada posici�n est� la pel�cula analizada y una lista de usuarios con opiniones similares, el listado de usuarios de cada pel�cula esta ordenado de menor a mayor diferencia.
	 */
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n);
	
	/**
	 * Retorna una lista con todos los tags de una pel�cula determinada
	 * @param idPelicula pel�cula que tiene los tags a buscar
	 * @return tags de la pel�cula idPelicula, ordenados de m�s a menos reciente por su timestamp
	 */
	public ILista<VOTag> tagsPeliculaSR(int idPelicula);
	
	/**
	 * Agregar una nueva operacion en el sistema
	 * @param nomOperacion nombre de la operaci�n
	 * @param tinicio tiempo de inicio de la operacion
	 * @param tfin tiempo final de la operacion
	 */
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin);
	
	/**
	 * Retorna una lista de operaciones realizadas sobre el sistema 
	 * @return lista de operaciones realizadas por el sistema
	 */
	public ILista<VOOperacion> darHistoralOperacionesSR();
	
	/**
	 * Limpia la lista de operaciones realizadas sobre el sistema 
	 * 
	 */
	public void limpiarHistorialOperacionesSR();
	
	/**
	 * Retorna una lista de las �ltimas n operaciones realizadas sobre el sistema
	 * @param n n�mero de operaciones
	 * @return lista de las �ltimas n operaciones realizadas en el orden inverso de ejecuci�n
	 */
	public ILista<VOOperacion> darUltimasOperaciones(int n);
	
	/**
	 * Borra las �ltimas n operaciones realizadas sobre el sistema
	 * @param n n�mero de operaciones
	 */
	public void borrarUltimasOperaciones(int n);
	
	/**
	 * Agrega una pel�cula al cat�logo de pel�culas
	 * @param titulo titulo de la pel�cula
	 * @param agno A�o de publicaci�n de la pel�cula
	 * @param generos G�neros que tiene la pel�cula
	 * @return Id de la pel�cula asignado por el sistema
	 */
	public long agregarPelicula(String titulo, int agno, String[] generos);
	
	/**
	 * Agrega un nuevo rating al historial del sistema de recomendaci�n
	 * @param idUsuario usuario del rating
	 * @param idPelicula pel�cula a la cual se le asigna el rating
	 * @param rating valoraci�n de la pel�cula
	 */
	public void agregarRating(int idUsuario, int idPelicula, double rating);


	public ILista<VOGeneroUsuario> usuariosActivosSR1(int n);
	
	
}
