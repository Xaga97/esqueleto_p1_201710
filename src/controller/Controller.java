package controller;

import model.data_structures.ILista;
import model.logic.SistemaRecomendacionPeliculas;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioGenero;

public class Controller {
	private static SistemaRecomendacionPeliculas manejador=new SistemaRecomendacionPeliculas();
	public static void cargarPeliculas(String ruta){
		manejador.cargarPeliculasSR(ruta);
	}
	public static void cargarRatings(String ruta){
		manejador.cargarRatingsSR(ruta);
	}
	public static void cargarTags(String ruta){
		manejador.cargarTagsSR(ruta);
	}
	public static int sizePeli(){
		return manejador.sizeMoviesSR();
	}
	public static int sizeUse(){
		return manejador.sizeUsersSR();
	}
	public static int sizeTags(){
		return manejador.sizeTagsSR();
	}
	public static ILista<VOGeneroPelicula> peliPop(int n){
		return manejador.peliculasPopularesSR(n);
	}
	public static ILista<VOPelicula> catalogOrdenaPeli(){
		return manejador.catalogoPeliculasOrdenadoSR();
	}
	public static ILista<VOGeneroPelicula> recGen(){
		return manejador.recomendarGeneroSR();
	}
	public static ILista<VOGeneroPelicula> opRatGen(){
		return manejador.opinionRatingsGeneroSR();
	}
	public static ILista<VOPeliculaPelicula> recPel(String ruta, int n){
		return manejador.recomendarPeliculasSR(ruta, n);
	}
	public static ILista<VORating> ratPel(long id){
		return manejador.ratingsPeliculaSR(id);
	}
	public static ILista<VOGeneroUsuario> usuAct(int n){
		return manejador.usuariosActivosSR(n);
	}
	public static ILista<VOUsuario> catUsOr(){
		return manejador.catalogoUsuariosOrdenadoSR();
	}
	public static ILista<VOGeneroPelicula> recTagsGen(int n){
		return manejador.recomendarTagsGeneroSR(n);
	}
	public static ILista<VOUsuarioGenero> opTagsGen(){
		return manejador.opinionTagsGeneroSR();
	}
	public static ILista<VOPeliculaUsuario> recUsSR(String ruta,int n){
		return manejador.recomendarUsuariosSR(ruta, n);
	}
	public static ILista<VOTag> tagsPeli(long id){
		return manejador.tagsPeliculaSR(id);
	}
	public static ILista<VOOperacion> histOp(){
		return manejador.darHistoralOperacionesSR();
	}
	public static void limpHis(){
		manejador.limpiarHistorialOperacionesSR();
	}
	public static ILista<VOOperacion> darUltOp(int n){
		return manejador.darUltimasOperaciones(n);
	}
	public static void borrUltOp(int n){
		manejador.borrarUltimasOperaciones(n);
	}
	public static long agrPeli(String titulo, int agno, String[] generos){
		return manejador.agregarPelicula(titulo, agno, generos);
	}
	public static void agrRat(int idUsuario, int idPelicula, double rating){
		manejador.agregarRating(idUsuario, idPelicula, rating);
	}
	public static void inicializar( )
	{
		manejador.inicializar();
	}
}
