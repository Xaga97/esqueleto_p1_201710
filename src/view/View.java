package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.ILista;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VORating;


public class View 
{
	private static void printMenu(){
		System.out.println("1. Cargar info");
		System.out.println("2. Usuarios activos");
		System.out.println("3. Usuarios ordenados");
		System.out.println("4. Lista peliculas ordenadas por tags");
		System.out.println("5. Lista  de usuario genero");
		System.out.println("6. Recomendar usuarios");
		System.out.println("7. tags por pelicula");
		System.out.println("8. Catalogo peliculas ordenado");
		System.out.println("9. Recomendar pelicula");
		System.out.println("10. Recomendar genero");
		System.out.println("11. Peliculas Populares");
		System.out.println("12. opinion rating genero");
		System.out.println("13. ratings peliculas");
		
		
	}
	
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		for(;;)
		{
			printMenu();
		    int option = sc.nextInt();
		    switch(option)
		    {
			  case 1: Controller.inicializar(); Controller.cargarPeliculas("/home/juliana/workspace/Esqueleto_P1_201710/Material de apoyo/ml-latest-small/movies.csv"); Controller.cargarRatings("/home/juliana/workspace/Esqueleto_P1_201710/Material de apoyo/ml-latest-small/ratings.csv"); Controller.cargarTags("/home/juliana/workspace/Esqueleto_P1_201710/Material de apoyo/ml-latest-small/tags.csv");			  
			  break;
			  
			  case 2: Controller.usuAct(50);
			  break;
			  
			  case 3: Controller.catUsOr();		  
			  break;
			  
			  case 4: Controller.recTagsGen(50);
			  break;
			  
			  case 5: Controller.opTagsGen();
			  break;
			  
			  case 6: Controller.recUsSR("/home/juliana/workspace/Esqueleto_P1_201710/Material de apoyo/ml-latest-small", 50);
			  break;
			  
			  case 7: Controller.tagsPeli(1);
			  break;
			 
			  case 8: Controller.catalogOrdenaPeli();
			  
			  break;
			  
			  case 9: Controller.recPel("/home/juliana/workspace/Esqueleto_P1_201710/Material de apoyo/ml-latest-small", 50);
			  break;
			  
			  case 10: ILista<VOGeneroPelicula> m=Controller.peliPop(10);
			  for(VOGeneroPelicula s:m){
				  for(VOPelicula t:s.getPeliculas())System.out.println(t);
			  }
			  break;
			  
			  case 11: 
			  default: System.out.println("--------- \n Invalid option !! \n---------");
			  
		  
		    }

	}
}
}
	
